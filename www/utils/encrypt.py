import hashlib

# from django.conf import settings
SECRET_KEY = 'django-insecure-n5u1j$8qsdbz*6et*6*vbnt5*p2bn@=#%vf+dzaq^_!vg%6-iq'


def md5_string(data_string):
    obj = hashlib.md5(SECRET_KEY.encode('utf-8'))
    obj.update(data_string.encode('utf-8'))
    return obj.hexdigest()


if __name__ == '__main__':
    data = '124'

    print(md5_string(data))
