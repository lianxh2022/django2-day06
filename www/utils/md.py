from django.utils.deprecation import MiddlewareMixin
from django.conf import settings
from django.shortcuts import redirect


class Context:
    def __init__(self, role, id, username):
        self.role = role
        self.id = id
        self.username = username


class AuthMiddleware(MiddlewareMixin):
    def is_white_url_by_path_info(self, request):
        if request.path_info in settings.NB_WHITE_URL:
            return True

    def is_white_url_by_name(self, request):
        if request.path_info in ['login', 'sms_login', 'send_sms']:
            return True

    def process_request(self, request):
        if self.is_white_url_by_path_info(request):
            return

        data_dict = request.session.get(settings.NB_SESSION_KEY)
        if not data_dict:
            return redirect(settings.NB_LOGIN_NAME)

        request.nb_name = Context(**data_dict)

    def process_view(self, request, callback, *args, **kwargs):
        if self.is_white_url_by_name(request):
            return

        data_dict = request.session.get(settings.NB_SESSION_KEY)
        if not data_dict:
            return redirect(settings.NB_LOGIN_NAME)

        request.nb_name = Context(**data_dict)
