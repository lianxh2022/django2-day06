from django.shortcuts import render, redirect
from django.conf import settings
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from www.forms import account
from www import models


def login(request):
    if request.method == "GET":
        form = account.LoginForms()
        return render(request, 'login.html', {'forms': form})

    form = account.LoginForms(data=request.POST)
    if not form.is_valid():
        return render(request, 'login.html', {'forms': form})

    data_dict = form.cleaned_data
    role = data_dict.pop('role')
    if role == "1":
        user_object = models.Administrator.objects.filter(active=1).filter(**data_dict).first()
    else:
        user_object = models.Administrator.objects.filter(active=1).filter(**data_dict).first()

    if not user_object:
        form.add_error({'password': '用户名或密码错误!'})
        return render(request, 'login.html', {'forms': form})

    mapping = {"1": 'ADMIN', '2': 'CUSTOMER'}
    request.session["user_info"] = {
        'role': mapping[role],
        'id': user_object.id,
        "username": user_object.username
    }

    return redirect(settings.HOME_URL)


def sms_login(request):
    if request.method == "GET":
        form = account.SmsLoginForms()
        return render(request, 'sms_login.html', {'forms': form})

    form = account.SmsLoginForms(data=request.POST)
    if not form.is_valid():
        return render(request, 'sms_login.html', {'forms': form})

    # data_dict = form.cleaned_data
    role = form.cleaned_data['role']
    mobile = form.cleaned_data['mobile']
    if role == "1":
        user_object = models.Administrator.objects.filter(mobile=mobile).filter(active=1).first()
    else:
        user_object = models.Administrator.objects.filter(mobile=mobile).filter(active=1).first()

    if not user_object:
        return JsonResponse({"state": False, 'msg': {'mobile': "手机号不能为空"}})

    mapping = {"1": 'ADMIN', '2': 'CUSTOMER'}
    request.session["user_info"] = {
        'role': mapping[role],
        'id': user_object.id,
        "username": user_object.username
    }
    return JsonResponse({'state': True, "msg": 'OK', 'data': settings.HOME_URL})


@csrf_exempt
def send_sms(request):
    form = account.SendSmsForm(data=request.POST)
    if not form.is_valid():
        return JsonResponse({'state': False, "msg": form.errors})

    return JsonResponse({'state': True, "msg": "OK"})

def home(request):
    username = request.nb_name.username

    return render(request,'home.html',{"username":username})
