import random

from django import forms
from django.core.validators import RegexValidator
from django.core.exceptions import ValidationError
from django_redis import get_redis_connection
from www.utils.encrypt import md5_string
from www import models
from www.utils import tencent
from www.utils.bootstrap import BootStrapForm


class LoginForms(BootStrapForm, forms.Form):
    # exclude_field_list = ['role']
    role = forms.ChoiceField(
        label='角色',
        required=True,
        choices=((2, '客户'), (1, '管理员'))
    )

    username = forms.CharField(
        label='用户名',
        required=True,
        widget=forms.TextInput
    )

    password = forms.CharField(
        label='密码',
        required=True,
        widget=forms.PasswordInput(render_value=True)
    )

    def clean_password(self):
        old = self.cleaned_data['password']
        return md5_string(old)


class SmsLoginForms(BootStrapForm, forms.Form):
    role = forms.ChoiceField(
        label='角色',
        required=True,
        choices=((2, '客户'), (1, '管理员'))
    )

    mobile = forms.CharField(
        label='手机号',
        required=True,
    )

    code = forms.CharField(
        label='验证码',
        required=True,
        widget=forms.PasswordInput
    )

    def clean_code(self):
        mobile = self.cleaned_data['mobile']
        code = self.cleaned_data['code']

        if not mobile:
            return code
        conn = get_redis_connection('default')
        cache_code = conn.get(mobile)
        if not cache_code:
            raise ValidationError('未发送或已失效')

        if code != cache_code.decode('utf-8'):
            raise ValidationError('验证码错误！')

        conn.delete(mobile)
        return code


class SendSmsForm(forms.Form):
    role = forms.ChoiceField(
        label='角色',
        required=True,
        choices=((2, '客户'), (1, '管理员'))
    )
    mobile = forms.CharField(
        label='手机号',
        required=True,
        widget=forms.TextInput,
        validators=[RegexValidator(r'^1[3578]\d{9}$', '手机号码格式不正确')]
    )

    def clean_mobile(self):
        role = self.cleaned_data.get('role')
        old = self.cleaned_data['mobile']
        if role == "1":
            exists = models.Administrator.objects.filter(active=1).filter(mobile=old).exists()
        else:
            exists = models.Customer.objects.filter(active=1).filter(mobile=old).exists()

        if not exists:
            raise ValidationError('手机号码不存在！')

        sms_code = random.randint(00000, 99999)
        print('生成的随机码为：', sms_code)
        if_ok = tencent.send_sms(old, sms_code)
        if not if_ok:
            raise ValidationError('短信发送失败！')

        conn = get_redis_connection('default')
        conn.set(old, sms_code, ex=5 * 60)

        return old
